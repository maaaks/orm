#!/usr/bin/env python

from distutils.core import setup

from setuptools import find_packages


setup(
    name='orm',
    version='0.0.1',
    author='Max Alibaev',
    install_requires=['asyncpg'],
    packages=find_packages(where='src'),
    package_dir={'': 'src'})
