from unittest import TestCase

from orm.record import Record


class TestRecord(TestCase):
    def setUp(self):
        self.record = Record(1, 2, 3)

    def test_length(self):
        self.assertEqual(3, len(self.record))

    def test_repr(self):
        self.assertEqual('Record(1, 2, 3)', repr(self.record))
