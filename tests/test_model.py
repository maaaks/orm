from orm.fields.integerfield import IntegerField
from orm.fields.stringfield import StringField
from orm.model import Model
from tests.test_render_expression import AbstractRenderingTestCase


class TestModel(AbstractRenderingTestCase):
    def test_simple(self):
        class MyModel(Model):
            a = IntegerField()
        self.assertRenders(MyModel, '"mymodel"')

    def test_custom_table(self):
        class MyModel(Model, table='my_model'):
            a = IntegerField()
        self.assertRenders(MyModel, '"my_model"')

    def test_model_inheritance(self):
        class MyModel1(Model):
            a = IntegerField()
        self.assertRenders(MyModel1, '"mymodel1"')
        self.assertEqual(1, len(MyModel1._meta.fields))
        self.assertIsInstance(MyModel1._meta.field('a'), IntegerField)

        class MyModel2(MyModel1):
            b = IntegerField()
        self.assertRenders(MyModel2, '"mymodel2"')
        self.assertEqual(2, len(MyModel2._meta.fields))
        self.assertIsInstance(MyModel2._meta.field('a'), IntegerField)
        self.assertIsInstance(MyModel2._meta.field('b'), IntegerField)

        class MyModel3(MyModel2):
            a = StringField(10)
            c = StringField(10)
        self.assertRenders(MyModel3, '"mymodel3"')
        self.assertEqual(3, len(MyModel3._meta.fields))
        self.assertIsInstance(MyModel3._meta.field('a'), StringField)
        self.assertIsInstance(MyModel3._meta.field('b'), IntegerField)
        self.assertIsInstance(MyModel3._meta.field('c'), StringField)
