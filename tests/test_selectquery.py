from orm.fields.integerfield import IntegerField
from orm.model import Model
from orm.queries.select import CTE, RecursiveCTE, SelectQuery, SelectQueryUnion

from tests.test_render_expression import AbstractRenderingTestCase


class _X(Model, table='x'):
    a = IntegerField()
    b = IntegerField()
    c = IntegerField()
    d = IntegerField()
    e = IntegerField()


class TestSelectQuery(AbstractRenderingTestCase):
    def test_empty(self):
        self.assertRenders(
            SelectQuery(_X),
            'select * from "x"')

    def test_fields(self):
        self.assertRenders(
            SelectQuery(_X).select(_X.a, _X),
            'select "x"."a", "x"."a", "x"."b", "x"."c", "x"."d", "x"."e" from "x"')

    def test_fields_append(self):
        self.assertRenders(
            SelectQuery(_X).select(_X.a, _X.b).select(..., _X),
            'select "x"."a", "x"."b", "x"."a", "x"."b", "x"."c", "x"."d", "x"."e" from "x"')

    def test_fields_replace(self):
        self.assertRenders(
            SelectQuery(_X).select(_X.a, _X),
            'select "x"."a", "x"."a", "x"."b", "x"."c", "x"."d", "x"."e" from "x"')

    def test_fields_with_alias(self):
        alias = _X.as_("xxx")
        self.assertRenders(
            SelectQuery(alias).select(alias.a, alias.b),
            'select "xxx"."a", "xxx"."b" from "x" as "xxx"')

    def test_where(self):
        self.assertRenders(
            SelectQuery(_X).select(_X.a, _X.b).where(_X.b > _X.c),
            'select "x"."a", "x"."b" from "x" where ("x"."b" > "x"."c")')

    def test_where_and_where(self):
        self.assertRenders(
            SelectQuery(_X).select(_X.a, _X.b).where(_X.b > _X.c).where(_X.c > _X.d),
            'select "x"."a", "x"."b" from "x" '
            'where (("x"."b" > "x"."c") and ("x"."c" > "x"."d"))')

    def test_order_by(self):
        self.assertRenders(
            SelectQuery(_X).select(_X.a).order_by(_X.e),
            'select "x"."a" from "x" order by "x"."e"')

    def test_limit(self):
        self.assertRenders(
            SelectQuery(_X).select(_X.a).limit(100),
            'select "x"."a" from "x" limit 100')

    def test_offset(self):
        self.assertRenders(
            SelectQuery(_X).select(_X.a).offset(100),
            'select "x"."a" from "x" offset 100')

    def test_page(self):
        self.assertRenders(
            SelectQuery(_X).select(_X.a).page(4, 100),
            'select "x"."a" from "x" limit 100 offset 300')

    def test_cte(self):
        t1 = CTE('T1', SelectQuery(_X).select(_X.a))
        t2 = CTE('T2', SelectQuery(_X).select(_X.b))
        self.assertRenders(
            SelectQuery(t1).join(t2, on=(_X.a == _X.b)),
            'with "T1" as (select "x"."a" from "x"), "T2" as (select "x"."b" from "x") '
            'select * from "T1" join "T2" on ("x"."a" = "x"."b")')

    def test_cte_recursive(self):
        q1 = SelectQuery(_X).select(_X.a).alias('CTE')
        q2 = SelectQuery(_X).select(_X.b).join(q1, on=(_X.b == q1.field('a')))
        cte = RecursiveCTE('CTE', SelectQueryUnion(q1, q2))
        self.assertRenders(
            SelectQuery(cte),
            'with recursive "CTE" as ('
            'select "x"."a" from "x" '
            'union '
            'select "x"."b" from "x" join "CTE" on ("x"."b" = "CTE"."a")'
            ') '
            'select * from "CTE"')


class TestSelectQueryUnion(AbstractRenderingTestCase):
    def test_union(self):
        self.assertRenders(
            SelectQueryUnion(SelectQuery(_X).select(_X.a), SelectQuery(_X).select(_X.a)),
            'select "x"."a" from "x" union select "x"."a" from "x"')
