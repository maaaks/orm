from unittest import TestCase

from orm.fields.integerfield import IntegerField
from orm.fields.stringfield import StringField
from orm.model import Model
from orm.record import Record


class _MyModel(Model):
    a = IntegerField()
    b = StringField(20)
    c = StringField(50, db_name='cc')
    
    extra_kwargs = {}
    
    def _post_init(self, **kwargs):
        self.extra_kwargs = kwargs


class TestModelFields_from_kwargs(TestCase):
    def setUp(self):
        self.model = _MyModel(a=1, b='2', c="3 ('three')", d=4, e=5)
    
    def test_fields_count(self):
        self.assertEqual(3, len(self.model._meta.fields))
    
    def test_field_a(self):
        field = self.model._meta.fields[0]
        with self.subTest('class'): self.assertIsInstance(field, IntegerField)
        with self.subTest('py_name'): self.assertEqual('a', field.py_name)
        with self.subTest('db_name'): self.assertEqual('a', field.db_name)
    
    def test_field_b(self):
        field = self.model._meta.fields[1]
        with self.subTest('class'): self.assertIsInstance(field, StringField)
        with self.subTest('db_type'): self.assertEqual('varchar(20)', field.db_type)
        with self.subTest('length'): self.assertEqual(20, field.length)
        with self.subTest('py_name'): self.assertEqual('b', field.py_name)
        with self.subTest('db_name'): self.assertEqual('b', field.db_name)
    
    def test_field_c(self):
        field = self.model._meta.fields[2]
        with self.subTest('class'): self.assertIsInstance(field, StringField)
        with self.subTest('db_type'): self.assertEqual('varchar(50)', field.db_type)
        with self.subTest('length'): self.assertEqual(50, field.length)
        with self.subTest('py_name'): self.assertEqual('c', field.py_name)
        with self.subTest('db_name'): self.assertEqual('cc', field.db_name)

    def test_repr(self):
        self.assertEqual("_MyModel(a=1, b='2', c='3 (\'three\')')", repr(self.model))
    
    def test_initialized_values(self):
        with self.subTest('a'):
            self.assertEqual(1, self.model.a)
        with self.subTest('b'):
            self.assertEqual('2', self.model.b)
        with self.subTest('c'):
            self.assertEqual("3 ('three')", self.model.c)
    
    def test_extra_kwargs_not_in_fields(self):
        self.assertFalse(hasattr(self.model, 'd'))
        self.assertFalse(hasattr(self.model, 'e'))
    
    def test_extra_kwargs_in_post_init(self):
        self.assertEqual({'d':4, 'e':5}, self.model.extra_kwargs)
