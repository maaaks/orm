from typing import Iterable
from unittest import TestCase

from orm.expressions.abstract import Expression
from orm.expressions.basic import Sql
from orm.fields.integerfield import IntegerField
from orm.fields.stringfield import StringField
from orm.indexes import Index, PrimaryKey
from orm.model import Model


class _User(Model):
    id = IntegerField(primary_key=True)
    login = StringField(100, index=True)
    password = StringField(100)
    
    _indexes = (
        Index(Sql('lower(login)'), password, name='login_and_password'),
    )


class TestModelMeta(TestCase):
    def setUp(self):
        self.model = _User()
    
    def assertExpressionsEqual(self, expected: Iterable[Expression], result: Iterable[Expression]):
        expected = tuple(x.__dict__ for x in expected)
        result = tuple(x.__dict__ for x in result)
        self.assertEqual(expected, result)
    
    def test_index_id(self):
        index = self.model._meta.indexes[0]
        self.assertIsInstance(index, PrimaryKey)
        self.assertExpressionsEqual((_User._meta.field('id'),), index.expressions)
    
    def test_index_login(self):
        index = self.model._meta.indexes[1]
        self.assertIsInstance(index, Index)
        self.assertExpressionsEqual((_User._meta.field('login'),), index.expressions)
    
    def test_index_login_and_password(self):
        index = self.model._meta.indexes[2]
        self.assertIsInstance(index, Index)
        self.assertEqual('login_and_password', index.name)
        self.assertExpressionsEqual((Sql('lower(login)'), _User._meta.field('password')), index.expressions)
    
    def test_primary_key(self):
        pk = self.model._meta.primary_key
        self.assertExpressionsEqual((_User._meta.field('id'),), pk.expressions)
