from orm.expressions.basic import Sql
from tests.test_render_expression import AbstractRenderingTestCase


class TestOperators(AbstractRenderingTestCase):
    def test_equal(self):
        self.assertRenders(
            Sql('a') == Sql('b'),
            '((a) = (b))')
    
    def test_not_equal(self):
        self.assertRenders(
            Sql('a') != Sql('b'),
            '((a) != (b))')
    
    def test_greater_than(self):
        self.assertRenders(
            Sql('a') > Sql('b'),
            '((a) > (b))')
    
    def test_greater_than_or_equal(self):
        self.assertRenders(
            Sql('a') >= Sql('b'),
            '((a) >= (b))')
    
    def test_less_than(self):
        self.assertRenders(
            Sql('a') < Sql('b'),
            '((a) < (b))')
    
    def test_less_than_or_equal(self):
        self.assertRenders(
            Sql('a') <= Sql('b'),
            '((a) <= (b))')
    
    def test_and(self):
        self.assertRenders(
            Sql('a') & Sql('b'),
            '((a) and (b))')
    
    def test_or(self):
        self.assertRenders(
            Sql('a') | Sql('b'),
            '((a) or (b))')
    
    def test_plus(self):
        self.assertRenders(
            Sql('a') + Sql('b'),
            '((a) + (b))')
    
    def test_minus(self):
        self.assertRenders(
            Sql('a') - Sql('b'),
            '((a) - (b))')
    
    def test_multiply(self):
        self.assertRenders(
            Sql('a') * Sql('b'),
            '((a) * (b))')
    
    def test_divide(self):
        self.assertRenders(
            Sql('a') / Sql('b'),
            '((a) / (b))')
    
    def test_not(self):
        self.assertRenders(
            ~Sql('a'),
            '(not (a))')
