import asyncpg._testbase
from asyncpg.transaction import Transaction

from orm.engines.asyncpg import AsyncpgDatabase
from orm.model import ModelMetaClass


class AbstractDatabaseTestCase(asyncpg._testbase.ConnectedTestCase):
    def setUp(self):
        super().setUp()
        self.db = AsyncpgDatabase(self.con)
        self.transaction: Transaction = self.db.transaction()
        self.loop.run_until_complete(self.transaction.start())
        ModelMetaClass.default_db = self.db

    def tearDown(self):
        self.loop.run_until_complete(self.transaction.rollback())
        super().tearDown()
