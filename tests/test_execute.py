from orm.record import Record
from tests.abstracttests import AbstractDatabaseTestCase


class TestExecute(AbstractDatabaseTestCase):
    async def test_fetch_all(self):
        actual = []
        async for x in self.db.execute('select 1 as x'):
            actual.append(x)
        self.assertEqual([Record(1)], actual)
    
    async def test_fetch_row(self):
        actual = await self.db.fetch_row('select 1 as x')
        self.assertEqual(Record(1), actual)
    
    async def test_fetch_value(self):
        actual = await self.db.fetch_value('select 1')
        self.assertEqual(1, actual)
