from orm.fields.foreignkeyfield import ForeignKeyField
from orm.fields.integerfield import IntegerField
from orm.fields.stringfield import StringField
from orm.model import Model
from orm.queries.select import SelectQuery
from tests.abstracttests import AbstractDatabaseTestCase
from tests.test_render_expression import AbstractRenderingTestCase


class _User(Model, table='user'):
    id = IntegerField(primary_key=True)
    name = StringField(50)


class _Category(Model, table='category'):
    id = IntegerField(primary_key=True)
    name = StringField(50)


class _Project(Model, table='project'):
    id = IntegerField(primary_key=True)
    name = StringField(50)
    
    user_id = IntegerField()
    user = ForeignKeyField(_User, user_id)
    
    category_name = StringField(50)
    category = ForeignKeyField(_Category, category_name, foreign_field=_Category._meta.field('name'))


class TestForeignKeyField_CustomLocal(AbstractRenderingTestCase, AbstractDatabaseTestCase):
    def test_foreign_field(self):
        self.assertIs(_Project._meta.field('user').foreign_field, _User._meta.field('id'))

    def test_render(self):
        self.assertRenders(_Project.user, '"project"."user_id"')

    async def test_join_on(self):
        await self.db._run_query('create table "user"(id int primary key, name text)')
        await self.db._run_query('create table "category"(id int primary key, name text unique)')
        await self.db._run_query('create table "project"(id int primary key, name text,'
                                 ' user_id int references "user"(id),'
                                 ' category_name text references "category"(name))')
        await self.db._run_query('insert into "user"(id, name) values(3, \'Max\')')
        await self.db._run_query('insert into "category"(id, name) values(2, \'Software\')')
        await self.db._run_query('insert into "project"(id, name, user_id, category_name)'
                                 ' values(1, \'ORM\', 3, \'Software\')')

        query = SelectQuery(_Project).select(_Project.id, _Project.name,
                                             _Project.user.id, _Project.user.name,
                                             _Project.category.id, _Project.category.name) \
                .join(_User, on=_Project.user_id==_User.id) \
                .join(_Category, on=_Project.category_name==_Category.name)
        self.assertRenders(query,
            'select'
            ' "project"."id", "project"."name",'
            ' "user"."id", "user"."name",'
            ' "category"."id", "category"."name"'
            ' from "project"'
            ' join "user" on ("project"."user_id" = "user"."id")'
            ' join "category" on ("project"."category_name" = "category"."name")')

        query._db = self.db
        async for project in query: pass
        self.assertEqual(project.id, 1)
        self.assertEqual(project.name, 'ORM')
        self.assertEqual(project.category.id, 2)
        self.assertEqual(project.category.name, 'Software')
        self.assertEqual(project.user.id, 3)
        self.assertEqual(project.user.name, 'Max')
