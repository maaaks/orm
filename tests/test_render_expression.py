from unittest import TestCase

from orm.expressions.abstract import Expression, RenderResult
from orm.expressions.basic import Sql
from orm.expressions.binary import Equal, GreaterThan, GreaterThanOrEqual, LessThan, \
    LessThanOrEqual, NotEqual
from orm.expressions.fn import fn
from orm.expressions.multi import And, Clause, CommaClause, Or
from orm.expressions.unary import IsNotNull, IsNull, Not
from orm.model import Model


class AbstractRenderingTestCase(TestCase):
    def assertRenders(self, expr: Expression, sql: str, params: tuple = ()):
        r = expr.render()
        self.assertEqual((sql, params), (r.sql, r.params))
        self.assertEqual((sql, params), r)
        self.assertEqual(RenderResult(sql, params), r)
        self.assertNotEqual(RenderResult('wrong sql', params), r)
        self.assertNotEqual(RenderResult(sql, ('wrong params',)), r)
        self.assertNotEqual(RenderResult('wrong sql', ('wrong params',)), r)
        self.assertNotEqual('wrong type', r)
        self.assertEqual(
            f'RenderResult({sql!r}, {params})' if params
            else (f'RenderResult({sql!r})' if sql else f'RenderResult()'),
            repr(r))


class TestRenderExpression(AbstractRenderingTestCase):
    def test_sql(self):
        self.assertRenders(
            Sql('1'),
            '(1)')
    
    def test_not(self):
        self.assertRenders(
            Not(Sql('1')),
            '(not (1))')

    def test_is_null(self):
        self.assertRenders(
            IsNull(Sql('1')),
            '((1) is null)')

    def test_is_not_null(self):
        self.assertRenders(
            IsNotNull(Sql('1')),
            '((1) is not null)')
    
    def test_not_and(self):
        self.assertRenders(
            Not(And(Sql('1'), Sql('2'))),
            '(not ((1) and (2)))')
    
    def test_and(self):
        self.assertRenders(
            And(Sql('1'), Sql('2'), Sql('3')),
            '((1) and (2) and (3))')
    
    def test_or(self):
        self.assertRenders(
            Or(Sql('1'), Sql('2'), Sql('3')),
            '((1) or (2) or (3))')
    
    def test_or_with_child_and(self):
        self.assertRenders(
            Or(Sql('1'), And(Sql('2'), Sql('3'))),
            '((1) or ((2) and (3)))')
    
    def test_clause(self):
        self.assertRenders(
            Clause(Sql('1'), Sql('2')),
            '((1) (2))')
    
    def test_comma_clause(self):
        self.assertRenders(
            CommaClause(Sql('1'), Sql('2')),
            '((1), (2))')
    
    def test_table(self):
        class MyModel(Model):
            pass
        
        self.assertRenders(
            MyModel,
            '"mymodel"')
    
    def test_table_with_custom_name(self):
        class MyModel(Model, table='my_custom_table'):
            pass
        
        self.assertRenders(
            MyModel,
            '"my_custom_table"')
    
    def test_binary_operators(self):
        for expr_class in (Equal, NotEqual, GreaterThan, GreaterThanOrEqual, LessThan, LessThanOrEqual):
            with self.subTest(expr_class.__name__):
                self.assertRenders(
                    expr_class(Sql('a'), Sql('b')),
                    f'((a) {expr_class._operator} (b))')

    def test_function(self):
        with self.subTest('without arguments'):
            self.assertRenders(
                fn.foo(),
                'foo()')
        with self.subTest('with arguments'):
            self.assertRenders(
                fn.foo(Sql('1'), Sql('2'), Sql('3')),
                'foo((1), (2), (3))')
