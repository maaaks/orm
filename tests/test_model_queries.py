from orm.fields.integerfield import IntegerField
from orm.fields.stringfield import StringField
from orm.model import Model
from tests.abstracttests import AbstractDatabaseTestCase


class _User(Model, table='user'):
    id = IntegerField(primary_key=True)
    login = StringField(100, index=True)
    password = StringField(100)


class TestModelQueries(AbstractDatabaseTestCase):
    def setUp(self):
        super().setUp()
        self.loop.run_until_complete(self.db.fetch_row('''
            create table "user" (id int, login text, password text)
        '''))
        self.loop.run_until_complete(self.db.fetch_row('''
            insert into "user" (id, login, password) values
                (1, 'john', 'AhT2ouGh'),
                (2, 'jack', 'Zie7oojo'),
                (3, 'jane', 'ieS9gae1')
        '''))
    
    async def test_select(self):
        rownum = 0
        
        async for obj in _User.select():
            with self.subTest(rownum):
                self.assertIsInstance(obj, _User)
            rownum += 1
        
        with self.subTest('Count'):
            self.assertEqual(3, rownum)

    async def test_select_fields(self):
        rownum = 0

        async for obj in _User.select(_User.id, _User.login):
            with self.subTest(rownum):
                self.assertIsInstance(obj, _User)
            rownum += 1

        with self.subTest('Count'):
            self.assertEqual(3, rownum)
    
    async def test_insert(self):
        async with self.db.transaction():
            await _User.insert().values(id=4, login='judy', password='quu8eiL2').execute()

            rownum = 0
            async for row in self.db.execute('''
                select id, login, password from "user" where id=4
            '''):
                self.assertEqual(4, row[0])
                self.assertEqual('judy', row[1])
                self.assertEqual('quu8eiL2', row[2])
                rownum += 1
            self.assertEqual(1, rownum)
