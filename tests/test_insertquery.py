from orm.fields.integerfield import IntegerField
from orm.model import Model
from orm.queries.insert import InsertQuery
from tests.test_render_expression import AbstractRenderingTestCase


class _X(Model, table='x'):
    a = IntegerField()
    b = IntegerField()
    c = IntegerField()
    d = IntegerField()
    e = IntegerField()


class _Y(Model, table='y'):
    a = IntegerField()
    b = IntegerField()
    c = IntegerField()
    d = IntegerField()
    e = IntegerField()


class TestInsertQuery(AbstractRenderingTestCase):
    def test_empty(self):
        self.assertRenders(
            InsertQuery(_X),
            'insert into "x" () values ()')

    def test_into(self):
        self.assertRenders(
            InsertQuery(_X).into(_Y),
            'insert into "y" () values ()')

    def test_values(self):
        self.assertRenders(
            InsertQuery(_X).values(a=10, b=20),
            'insert into "x" (a, b) values ($1, $2)',
            (10, 20))

    def test_values_append(self):
        self.assertRenders(
            InsertQuery(_X).values(a=10, b=20).values(c=30, d=40),
            'insert into "x" (a, b, c, d) values ($1, $2, $3, $4)',
            (10, 20, 30, 40))

    def test_values_update(self):
        self.assertRenders(
            InsertQuery(_X).values(a=10, b=20).values(b=25, c=30, d=40),
            'insert into "x" (a, b, c, d) values ($1, $2, $3, $4)',
            (10, 25, 30, 40))
