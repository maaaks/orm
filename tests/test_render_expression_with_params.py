from orm.expressions.basic import Param
from orm.expressions.multi import And, Clause, CommaClause, Or
from orm.expressions.unary import Not
from tests.test_render_expression import AbstractRenderingTestCase


class TestRenderExpression(AbstractRenderingTestCase):
    def test_param(self):
        self.assertRenders(
            Param('a'),
            '$1',
            ('a',))
    
    def test_not(self):
        self.assertRenders(
            Not(Param('a')),
            '(not $1)',
            ('a',))
    
    def test_and(self):
        self.assertRenders(
            And(Param('a'), Param('b')),
            '($1 and $2)',
            ('a', 'b'))
    
    def test_or(self):
        self.assertRenders(
            Or(Param('a'), Param('b')),
            '($1 or $2)',
            ('a', 'b'))
    
    def test_complex(self):
        self.assertRenders(
            Or(
                And(
                    Param('a'),
                    Param('b'),
                    Or(
                        Param('c'),
                        Param('d'),
                        Param('e'),
                    ),
                ),
                Param('f'),
                And(
                    Param('g'),
                    Param('h'),
                )
            ),
            '(($1 and $2 and ($3 or $4 or $5)) or $6 or ($7 and $8))',
            ('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'))
    
    def test_clause(self):
        self.assertRenders(
            Clause(Param('a'), Param('b')),
            '($1 $2)',
            ('a', 'b'))
    
    def test_comma_clause(self):
        self.assertRenders(
            CommaClause(Param('a'), Param('b')),
            '($1, $2)',
            ('a', 'b'))
