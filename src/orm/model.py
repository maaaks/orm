from __future__ import annotations

from abc import ABC
from dataclasses import dataclass
from typing import Generic, Iterable, Iterator, Type, TypeVar

from orm.database import Database
from orm.expressions.abstract import Expression, RenderResult
from orm.indexes import Index, PrimaryKey


class FieldRef(Expression):
    def __init__(self, field: Field, *, parent: FieldRef|Model|ModelAlias = None):
        self.__parent: FieldRef|Model|ModelAlias = parent
        self.__field: Field|None = field

    def __repr__(self):
        result = self.__root.__name__
        for item in self.__chain:
            result += '.' + item.__field.py_name
        return result

    def __call__(self, obj: Model, value):
        for item in self.__chain[:-1]:
            next_obj = getattr(obj, item.__field.py_name)
            if next_obj is None:
                next_obj = item.__field.foreign_model()
                setattr(obj, item.__field.py_name, next_obj)
            obj = next_obj
        setattr(obj, self.__field.py_name, value)

    def __iter__(self) -> Iterable[FieldRef]:
        from orm.fields.foreignkeyfield import ForeignKeyField
        if not isinstance(self.__field, ForeignKeyField):
            raise TypeError('Only ForeignKeyField-based FieldRefs are iterable')
        for field in self.__field.foreign_model._meta.fields:
            if not isinstance(field, ForeignKeyField):
                yield FieldRef(field, parent=self)

    @property
    def __root(self) -> Type[Model]:
        return self.__chain[0].__parent

    @property
    def __chain(self) -> tuple[FieldRef]:
        chain = self,
        while isinstance(chain[0].__parent, FieldRef):
            chain = chain[0].__parent, *chain
        return chain

    def __getattr__(self, key):
        from orm.fields.foreignkeyfield import ForeignKeyField
        if isinstance(self.__field, ForeignKeyField):
            next_item = self.__field.foreign_model._meta.field(key)
            return FieldRef(next_item, parent=self)
        raise AttributeError(key)

    def _render(self, r: RenderResult):
        from orm.queries.select import SelectQuery
        if isinstance(self.__parent, ModelAlias):
            r.sql += f'"{self.__parent._ModelAlias__alias}"."{self.__field.db_name}"'
        elif isinstance(self.__parent, SelectQuery):
            r.sql += f'"{self.__parent._alias}"."{self.__field.db_name}"'
        else:
            r.sql += f'"{self.__field.model._meta.table}"."{self.__field.db_name}"'


T = TypeVar('T')
class Field(Generic[T], ABC):
    py_type: type = NotImplemented
    db_type: str = NotImplemented

    def __init__(self, *, db_name: str = None, index: bool = False, primary_key: bool = False):
        self.model: Model = None
        self.py_name: str = None
        self.db_name: str = db_name
        self.index: bool = index
        self.primary_key: bool = primary_key

    def __repr__(self):
        if self.py_name:
            return f'<{self.__class__.__name__}: {self.model.__name__}.{self.py_name}>'
        else:
            return f'<Unbound {self.__class__.__name__}>'

    def __set__(self, instance: Model, value: T):
        instance.__dict__[self.py_name] = value

    def __get__(self, instance: Model|None, owner: Type[Model]) -> T:
        if instance is not None:
            return instance.__dict__[self.py_name]
        return FieldRef(self, parent=self.model)


@dataclass(frozen=True)
class ModelMetaInfo:
    """
    Class for ``model._meta``.
    """
    table: str
    """Name of table in database."""
    
    fields: tuple[Field,...]
    """All fields in the same order as they are defined in the model."""
    
    indexes: tuple[Index,...]
    primary_key: Index = None
    
    _database: Database = None
    """Database configuration to use with this model."""
    
    @property
    def database(self) -> Database:
        return self._database or ModelMetaClass.default_db

    def field(self, name: str) -> Field:
        for field in self.fields:
            if field.py_name == name:
                return field
        raise KeyError(name)


class ModelMetaClass(type, Iterable[Field], Expression):
    default_db: Database
    registered_models: set[Model] = set()

    @classmethod
    def __init_subclass__(cls, **kwargs):  # pragma: no cover
        ModelMetaClass.registered_models.add(cls)

    def __new__(cls, name, bases, namespace, **kwargs):
        fields: list[Field] = []
        indexes: list[Index] = []
        primary_key: PrimaryKey = None

        # Pre-fill variables from bases
        for base in bases:
            if issubclass(base, Model) and base is not Model:
                for field in base._meta.fields:
                    if not isinstance(namespace.get(field.py_name), Field):
                        fields.append(field)
                indexes += base._meta.indexes
                primary_key = base._meta.primary_key or primary_key
        
        # Get or generate table name
        table = kwargs.get('table', name.lower())
        
        # Iterate through all Field-based instances in the class
        for field_name, field in namespace.items():
            if isinstance(field, Field):
                # Add field to list
                fields.append(field)
                
                # Use the name used for this field in the class as field.py_name
                # and, if not provided otherwise, as field.db_name
                field.py_name = field_name
                if not field.db_name:
                    field.db_name = field_name

                # Instantiate index for the field if neccessary
                if field.primary_key:
                    assert primary_key is None, 'Multiple primary keys declared'
                    primary_key = PrimaryKey(field)
                    indexes.append(primary_key)
                elif field.index:
                    indexes.append(Index(field))
        
        # Add custom indexes
        indexes += namespace.pop('_indexes', [])
        
        namespace['_meta'] = ModelMetaInfo(
            table=table,
            fields=tuple(fields),
            indexes=tuple(indexes),
            primary_key=primary_key,
        )
        
        model_class = type.__new__(cls, name, bases, namespace)
        for field in fields:
            if field.model is None:
                field.model = model_class
        
        return model_class

    def __hash__(self):
        return hash(tuple(self.__dict__.items()))
    
    def _render(self, r: RenderResult):
        r.sql += f'"{self._meta.table}"'
    
    def __iter__(self) -> Iterator[Field]:
        from orm.fields.foreignkeyfield import ForeignKeyField
        for field in self._meta.fields:
            if not isinstance(field, ForeignKeyField):
                yield FieldRef(field, parent=self)

    def as_(cls, alias: str) -> ModelAlias:
        return ModelAlias(cls, alias)


class Model(metaclass=ModelMetaClass):
    _meta: ModelMetaInfo = None
    _indexes: tuple[Index,...] = ()
    
    def __init__(self, *args, **kwargs):
        # If given a Record, extract its fields to a dict
        if args:
            assert len(args) == 1
            assert len(kwargs) == 0
            kwargs = dict(**args[0])

        # For each field, try to fill it using keyword arguments
        # When found, remove argument from kwargs
        # When not found, use None as the field value
        for field in self._meta.fields:
            value = kwargs.pop(field.py_name, None)
            setattr(self, field.py_name, value)
        
        # Call custom post-initialization hook using all extra arguments
        # By default, this will call TypeError on argument number mismatch
        self._post_init(**kwargs)
    
    def _post_init(self):
        """
        This method is called at the end of an instance creation,
        and all unused keyword arguments from the constructor are being passed here.
        """
    
    def __repr__(self):
        """
        Represents the instance as a string like ``MyModel()`` or ``MyModel(a=1, b=2)``.
        All fields that are `None` are omitted in the representation.
        """
        parts = []
        for field in self._meta.fields:
            value = getattr(self, field.py_name)
            if value:
                if isinstance(value, str):
                    value = "'" + value.replace('\\', '\\\\').replace("'", "\'") + "'"
                else:
                    value = repr(value)
                parts.append(f'{field.py_name}={value}')
        
        return self.__class__.__name__ + '(' + ', '.join(parts) + ')'
    
    @classmethod
    def select(cls, *expressions: Expression) -> 'SelectQuery':
        from orm.queries.select import SelectQuery
        if not expressions:
            expressions = cls
        return SelectQuery(cls, db=cls._meta.database).select(*expressions)
    
    @classmethod
    def insert(cls) -> 'InsertQuery':
        from orm.queries.insert import InsertQuery
        return InsertQuery(cls, db=cls._meta.database)


class ModelAlias:
    def __init__(self, model_class: ModelMetaClass, alias: str):
        self.__model_class: Type[Model] = model_class
        self.__alias: str = alias

    def __getattr__(self, key) -> FieldRef:
        field = self.__model_class._meta.field(key)
        return FieldRef(field, parent=self)
