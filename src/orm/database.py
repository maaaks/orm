from abc import ABC, abstractmethod
from contextlib import AbstractAsyncContextManager
from dataclasses import dataclass
from typing import AsyncIterable, Iterable

from orm.record import Record


@dataclass
class DbParams:
    hostname: str = None
    port: int = None
    username: str = None
    password: str = None
    database: str = None


class Database(ABC):
    @abstractmethod
    def connect(self, params: DbParams):
        """"""
    
    @abstractmethod
    def transaction(self) -> AbstractAsyncContextManager:
        """"""
    
    @abstractmethod
    async def _run_query(self, query: str, params: Iterable[str] = ()) -> Iterable[Record]:
        """"""
    
    @abstractmethod
    def _run_query_with_cursor(self, query: str, params: Iterable[str] = ()) \
            -> AsyncIterable[Record]:
        """"""
    
    async def execute(self, query: str, params: Iterable[str] = (),
        *, with_cursor: bool = False
    ) -> AsyncIterable[Record]:
        if with_cursor:
            async with self.transaction():
                async for row in self._run_query_with_cursor(query, params):
                    yield row
        else:
            for row in await self._run_query(query, params):
                yield row
    
    async def fetch_row(self, query: str, params: Iterable[str] = ()) -> Record:
        async for row in self.execute(query, params, with_cursor=False):
            return row
    
    async def fetch_value(self, query: str, params: Iterable[str] = ()):
        row = await self.fetch_row(query, params)
        return row[0]
