from typing import Iterable

import asyncpg
import asyncpg.connection

from orm.database import Database, DbParams
from orm.record import Record


class AsyncpgDatabase(Database):
    def __init__(self, connecton: asyncpg.connection.Connection = None):
        self._connection: asyncpg.connection.Connection = connecton
    
    async def connect(self, params: DbParams):  # pragma: no cover
        self._connection = await asyncpg.connect(
            host=params.hostname, port=params.port,
            user=params.username, password=params.password,
            database=params.database)
    
    def transaction(self):
        return self._connection.transaction()
    
    async def _run_query(self, query: str, params: Iterable[str] = ()):
        rows = await self._connection.fetch(query, *params)
        return (Record(*row) for row in rows)
    
    async def _run_query_with_cursor(self, query: str, params: Iterable[str] = ()):
        async for row in self._connection.cursor(query, *params):
            yield Record(*row)
