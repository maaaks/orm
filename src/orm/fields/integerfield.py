from orm.model import Field


class IntegerField(Field[int]):
    py_type = int
    db_type = 'int'
