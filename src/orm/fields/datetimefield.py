from datetime import datetime

from orm.model import Field


class DateTimeField(Field[datetime]):
    py_type = datetime
    db_type = 'datetime'
