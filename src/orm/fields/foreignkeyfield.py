from __future__ import annotations

from typing import Type, TypeVar

from orm.expressions.abstract import Expression, RenderResult
from orm.model import Field, Model


T = TypeVar('T')
class ForeignKeyField(Field):
    def __init__(self, foreign_model: Type[Model], local_field: Field, *,
                 foreign_field: Field = None, on: Expression = None):
        """
        :param foreign_model: Model which is referenced by this field.
        :param local_field: Local field that stores id of foreign model.
        :param foreign_field: Foreign field to be used as foreign model's identifier.
        :param on: Expression for joining tables.
        """
        super().__init__()
        self.py_type = foreign_model
        self.foreign_model: Type[Model] = foreign_model
        
        # Set local field
        self.local_field: Field = local_field

        # Set foreign field
        self.foreign_field: Field
        if foreign_field:
            assert foreign_field.model is foreign_model
            self.foreign_field = foreign_field
        else:
            assert len(foreign_model._meta.primary_key.expressions) == 1, \
                'Multicolumn primary keys are not supported yet.'
            self.foreign_field = foreign_model._meta.primary_key.expressions[0]
            assert isinstance(self.foreign_field, Field), \
                'Expressions in primary keys are not supported yet.'
        
        # Set expression for joining
        self.on: Expression = on

    def __getattribute__(self, key):
        if key in ('db_type', 'db_name'):
            return getattr(self.local_field, key)
        return object.__getattribute__(self, key)

    def _render(self, r: RenderResult):
        self.local_field.render(r)
