from orm.model import Field


class StringField(Field[str]):
    py_type = str
    db_type = 'varchar'

    def __init__(self, length: int, **kwargs):
        self.db_type = f'varchar({length:d})'
        self.length: int = length

        super().__init__(**kwargs)
