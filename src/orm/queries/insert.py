from collections import OrderedDict
from typing import Any, Generic, TypeVar

from orm.database import Database
from orm.expressions.abstract import Expression, RenderResult
from orm.expressions.basic import Param


T = TypeVar('T')
class InsertQuery(Expression, Generic[T]):
    def __init__(self, into: Expression, *, db: Database = None):
        self._into: Expression = into
        self._values: OrderedDict[str,Any] = OrderedDict()
        
        self._db: Database = db
    
    def into(self, into: Expression):
        self._into = into
        return self
    
    def values(self, **values):
        for k, v in values.items():
            self._values[k] = v
        return self
    
    def _render(self, r: RenderResult):
        r.sql += 'insert into '
        
        # Table name
        self._into.render(r)
        
        # Column names
        r.sql += ' (' + ', '.join(self._values.keys()) + ')'
        
        # Values
        r.sql += ' values ('
        for i, value in enumerate(self._values.values()):
            if i != 0:
                r.sql += ', '
            Param(value).render(r)
        r.sql += ')'
    
    async def execute(self):
        r = self.render()
        await self._db.fetch_row(r.sql, r.params)
