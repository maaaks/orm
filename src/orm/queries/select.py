from __future__ import annotations

from contextlib import suppress
from enum import Enum, auto
from typing import AsyncIterator, Generic, TypeVar

from orm.database import Database
from orm.expressions.abstract import Expression, RenderResult
from orm.expressions.fn import SqlFunctionCall
from orm.expressions.multi import And
from orm.model import FieldRef


class Join:
    def __init__(self, model: 'Model', on: Expression, join_type: JoinType):
        self.model: 'Model'|CTE = model
        self.on: Expression = on
        self.type: JoinType = join_type


class JoinType(Enum):
    Inner = auto()
    Full = auto()
    Left = auto()
    Right = auto()


T = TypeVar('T')
class SelectQuery(Expression, Generic[T]):
    def __init__(self, _from: CTE|Expression, db: Database = None):
        self._from: tuple[Expression|Join|CTE] = _from,
        self._select: tuple[Expression,...] = ()
        self._where: Expression|None = None
        self._order_by: tuple[Expression,...] = ()
        self._limit: int|None = None
        self._offset: int|None = None

        self._alias: str|None = None
        
        self._db: Database = db

    def __repr__(self):
        return f'<{self.__class__.__name__} from {self._from.render().sql}>'

    def select(self, *select: Expression) -> SelectQuery:
        from orm.model import Model
        if select[0] is ...:
            select = *self._select, *select[1:]
        self._select = ()
        for item in select:
            if isinstance(item, type) and issubclass(item, Model):
                self._select = *self._select, *item
            else:
                self._select = *self._select, item
        return self
    
    def join(self, model: 'Model', on: Expression = None, join_type: JoinType = JoinType.Inner) \
            -> SelectQuery:
        self._from = *self._from, Join(model, on, join_type)
        return self
    
    def where(self, condition: Expression|tuple[Expression,...]) -> SelectQuery:
        if self._where is None:
            self._where = condition
        else:
            self._where = And(self._where, condition)
        return self

    def order_by(self, *order_by: Expression) -> SelectQuery:
        if order_by[0] is ...:
            self._order_by = *self._order_by, *order_by
        else:
            self._order_by = order_by
        return self

    def limit(self, limit: int|None) -> SelectQuery:
        self._limit = limit
        return self

    def offset(self, offset: int|None) -> SelectQuery:
        self._offset = offset
        return self

    def page(self, page: int, page_size: int) -> SelectQuery:
        self._limit = page_size
        self._offset = (page - 1) * page_size
        return self

    def alias(self, alias: str) -> SelectQuery:
        self._alias = alias
        return self

    def field(self, key: str) -> FieldRef:
        from orm.model import ModelAlias, ModelMetaClass

        for source in self._from:
            if type(model := source) is ModelMetaClass:
                for field in model._meta.fields:
                    if field.py_name == key:
                        return FieldRef(field, parent=self)

            elif isinstance(alias := source, ModelAlias):
                for field in alias._ModelAlias__model_class._meta.fields:
                    if field.py_name == key:
                        return FieldRef(field, parent=self)

            elif isinstance(cte := source, CTE):
                queries = cte.query.queries if isinstance(cte.query, SelectQueryUnion) \
                    else (cte.query,)
                with suppress(KeyError):
                    for query in queries:
                        return FieldRef(query.field(key)._FieldRef__field, parent=self)

            elif isinstance(join := source, Join):
                if isinstance(cte := join.model, CTE):
                    queries = cte.query.queries if isinstance(cte.query, SelectQueryUnion) \
                        else (cte.query,)
                    with suppress(KeyError):
                        for query in queries:
                            return FieldRef(query.field(key)._FieldRef__field, parent=self)

                elif isinstance(query := join.model, SelectQuery):
                    with suppress(KeyError):
                        return FieldRef(query.field(key)._FieldRef__field, parent=self)

                else:
                    for field in join.model._meta.fields:
                        if field.py_name == key:
                            return FieldRef(field, parent=self)

            raise KeyError(key)

    
    def _render(self, r: RenderResult):
        from orm.model import ModelAlias, ModelMetaClass

        # CTE clause
        all_cte = []
        for source in self._from:
            if isinstance(source, CTE):
                all_cte.append(source)
            elif isinstance(source, Join) and isinstance(source.model, CTE):
                all_cte.append(source.model)
        if all_cte:
            r.sql += 'with '
            for i, cte in enumerate(all_cte):
                if i != 0:
                    r.sql += ', '
                if isinstance(cte, RecursiveCTE):
                    r.sql += 'recursive '
                r.sql += f'"{cte.alias}" as ('
                cte.query.render(r)
                r.sql += ')'
            r.sql += ' '

        # SELECT clause
        r.sql += 'select '
        if self._select:
            for i, expr in enumerate(self._select):
                if i != 0:
                    r.sql += ', '
                expr.render(r)
        else:
            r.sql += '*'
        
        # FROM clause
        r.sql += ' from '
        for source in self._from:
            if type(source) is ModelMetaClass:
                r.sql += f'"{source._meta.table}"'
            elif isinstance(source, ModelAlias):
                r.sql += f'"{source._ModelAlias__model_class._meta.table}" as "{source._ModelAlias__alias}"'
            elif isinstance(source, CTE):
                r.sql += f'"{source.alias}"'
            elif isinstance(join := source, Join):
                r.sql += {
                    JoinType.Inner: ' join ',
                    JoinType.Full: ' full join ',
                    JoinType.Left: ' left join ',
                    JoinType.Right: ' right join ',
                }[join.type]
                if isinstance(join.model, CTE):
                    r.sql += '"' + join.model.alias + '"'
                elif isinstance(join.model, SelectQuery):
                    r.sql += '"' + join.model._alias + '"'
                else:
                    r.sql += '"' + join.model._meta.table + '"'
                r.sql += ' on '
                join.on.render(r)
        
        # WHERE clause
        if self._where is not None:
            r.sql += ' where '
            self._where.render(r)

        # ORDER BY clause
        if self._order_by:
            r.sql += ' order by '
            for i, expr in enumerate(self._order_by):
                if i != 0:
                    r.sql += ', '
                expr.render(r)

        # LIMIT, OFFSET clauses
        if self._limit is not None:
            r.sql += f' limit {self._limit}'
        if self._offset is not None:
            r.sql += f' offset {self._offset}'
    
    async def __aiter__(self) -> AsyncIterator[T]:
        from orm.model import FieldRef

        r = self.render()
        result = self._db.execute(r.sql, r.params)
        async for row in result:
            obj: 'Model' = self._from[0]()
            for expr, value in zip(self._select, row):
                if isinstance(expr, FieldRef):
                    expr.__call__(obj, value)
                elif isinstance(expr, SqlFunctionCall):
                    setattr(obj, expr.alias, value)
                else:
                    raise TypeError(type(expr))
            yield obj

    async def one(self) -> T:
        async for result in self:
            return result
        raise StopIteration


class SelectQueryUnion(Expression):
    def __init__(self, *queries: SelectQuery):
        self.queries: tuple[SelectQuery, ...] = queries

    def _render(self, r: RenderResult):
        self.queries[0].render(r)
        for query in self.queries[1:]:
            r.sql += ' union '
            query.render(r)


class SelectQueryAlias:
    def __init__(self, query: SelectQuery, alias: str):
        self.__query: SelectQuery = query
        self.__alias: str = alias

    def __getattr__(self, key: str) -> 'FieldRef':
        from orm.model import Field, FieldRef, Model, ModelAlias, ModelMetaClass

        for source in self.__query._from:

            if type(source) is ModelMetaClass:
                for field in source._meta.fields:
                    if field.py_name == key:
                        return FieldRef(field, parent=self)

            elif isinstance(alias := source, ModelAlias):
                for field in alias._ModelAlias__model_class.fields:
                    if field.py_name == key:
                        return FieldRef(field, parent=self)

            elif isinstance(cte := source, CTE):
                for field in cte.query._select:
                    if isinstance(field, Field):
                        if field.py_name == key:
                            return FieldRef(field, parent=self)
                    raise TypeError(repr(field))

            elif isinstance(join := source, Join):
                if isinstance(model := join.model, Model):
                    for field in model._meta.fields:
                        if field.py_name == key:
                            return FieldRef(field, parent=self)
                elif isinstance(cte := join.model, CTE):
                    for field in cte.query._select:
                        if isinstance(field, Field):
                            if field.py_name == key:
                                return FieldRef(field, parent=self)
                        raise TypeError(repr(field))
                raise TypeError(repr(join.model))

            raise TypeError(repr(source))


class CTE:
    def __init__(self, alias: str, query: SelectQuery|SelectQueryUnion):
        self.alias: str = alias
        self.query: SelectQuery|SelectQueryUnion = query

    def __repr__(self):
        return f'<{self.__class__.__name__}: {self.alias}>'


class RecursiveCTE(CTE):
    pass
