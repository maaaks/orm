from __future__ import annotations

from orm.expressions.abstract import Expression, RenderResult


class SqlFunctionCall(Expression):
    def __init__(self, funcname: str, *args: Expression):
        self.funcname: str = funcname
        self.args: tuple[Expression, ...] = args
        self.alias: str|None = None

    def as_(self, alias: str) -> SqlFunctionCall:
        self.alias = alias
        return self

    def _render(self, r: RenderResult):
        r.sql += self.funcname + '('
        for i, arg in enumerate(self.args):
            if i > 0:
                r.sql += ', '
            arg.render(r)
        r.sql += ')'
        if self.alias:
            r.sql += f' as "{self.alias}"'


class SqlFunction:
    def __init__(self, name: str):
        self.name: str = name

    def __call__(self, *args: Expression) -> SqlFunctionCall:
        return SqlFunctionCall(self.name, *args)


class SqlFunctionGenerator:
    def __getattr__(self, name):
        return SqlFunction(name)


fn = SqlFunctionGenerator()
