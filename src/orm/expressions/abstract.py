from __future__ import annotations

from abc import ABC, abstractmethod


class Expression(ABC):
    def render(self, r: RenderResult = None) -> RenderResult:
        r = r or RenderResult()
        self._render(r)
        return r
    
    @abstractmethod
    def _render(self, r: RenderResult) -> None:
        """"""
    
    def __eq__(self, other: Expression):
        if other is None:
            from orm.expressions.unary import IsNull
            return IsNull(self)
        from orm.expressions.binary import Equal
        return Equal(self, other)
    
    def __ne__(self, other: Expression):
        if other is None:
            from orm.expressions.unary import IsNotNull
            return IsNotNull(self)
        from orm.expressions.binary import NotEqual
        return NotEqual(self, other)
    
    def __gt__(self, other: Expression):
        from orm.expressions.binary import GreaterThan
        return GreaterThan(self, other)
    
    def __ge__(self, other: Expression):
        from orm.expressions.binary import GreaterThanOrEqual
        return GreaterThanOrEqual(self, other)
    
    def __lt__(self, other: Expression):
        from orm.expressions.binary import LessThan
        return LessThan(self, other)
    
    def __le__(self, other: Expression):
        from orm.expressions.binary import LessThanOrEqual
        return LessThanOrEqual(self, other)
    
    def __and__(self, other: Expression):
        from orm.expressions.multi import And
        return And(self, other)
    
    def __or__(self, other: Expression):
        from orm.expressions.multi import Or
        return Or(self, other)
    
    def __add__(self, other: Expression):
        from orm.expressions.binary import Plus
        return Plus(self, other)
    
    def __sub__(self, other: Expression):
        from orm.expressions.binary import Minus
        return Minus(self, other)
    
    def __mul__(self, other: Expression):
        from orm.expressions.binary import Multiply
        return Multiply(self, other)
    
    def __truediv__(self, other: Expression):
        from orm.expressions.binary import Divide
        return Divide(self, other)
    
    def __invert__(self):
        from orm.expressions.unary import Not
        return Not(self)


class RenderResult:
    def __init__(self, sql: str = '', params: tuple[str,...] = ()):
        self.sql: str = sql
        self.params: tuple[str,...] = params
    
    def add_param(self, value) -> int:
        self.params = *self.params, value
        return len(self.params)

    def __repr__(self):
        if self.sql and self.params:
            return f'{self.__class__.__name__}({self.sql!r}, {self.params!r})'
        elif self.sql:
            return f'{self.__class__.__name__}({self.sql!r})'
        else:
            return f'{self.__class__.__name__}()'

    def __eq__(self, other):
        if isinstance(other, RenderResult):
            return (self.sql, self.params) == (other.sql, other.params)
        if type(other) is tuple:
            return (self.sql, self.params) == other
        return False


class UnaryExpression(Expression, ABC):
    def __init__(self, expression: Expression):
        self.child: Expression = expression


class BinaryExpression(Expression, ABC):
    _operator: str = None
    
    def __init__(self, child1: Expression, child2: Expression):
        self.child1: Expression = child1
        self.child2: Expression = child2
    
    def _render(self, r: RenderResult):
        r.sql += '('
        self.child1.render(r)
        r.sql += ' ' + self._operator + ' '
        self.child2.render(r)
        r.sql += ')'


class MultiExpression(Expression):
    _separator: str = None
    
    def __init__(self, *expressions: Expression):
        self.children: tuple[Expression,...] = tuple(expressions)
    
    def _render(self, r: RenderResult):
        r.sql += '('
        for i, child in enumerate(self.children):
            if i != 0:
                r.sql += self._separator
            child.render(r)
        r.sql += ')'
