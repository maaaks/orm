from orm.expressions.abstract import Expression, RenderResult


class Desc(Expression):
    def __init__(self, child: Expression):
        self.child: Expression = child

    def _render(self, r: RenderResult):
        self.child.render(r)
        r.sql += ' desc'

