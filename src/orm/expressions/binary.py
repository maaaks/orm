from orm.expressions.abstract import BinaryExpression


class Equal(BinaryExpression):
    _operator = '='


class NotEqual(BinaryExpression):
    _operator = '!='


class GreaterThan(BinaryExpression):
    _operator = '>'


class GreaterThanOrEqual(BinaryExpression):
    _operator = '>='


class LessThan(BinaryExpression):
    _operator = '<'


class LessThanOrEqual(BinaryExpression):
    _operator = '<='


class Plus(BinaryExpression):
    _operator = '+'


class Minus(BinaryExpression):
    _operator = '-'


class Multiply(BinaryExpression):
    _operator = '*'


class Divide(BinaryExpression):
    _operator = '/'
