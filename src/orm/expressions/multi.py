from orm.expressions.abstract import MultiExpression


class And(MultiExpression):
    _separator = ' and '


class Or(MultiExpression):
    _separator = ' or '


class Clause(MultiExpression):
    _separator = ' '


class CommaClause(MultiExpression):
    _separator = ', '
