from orm.expressions.abstract import RenderResult, UnaryExpression


class Not(UnaryExpression):
    def _render(self, r: RenderResult):
        r.sql += '(not '
        self.child.render(r)
        r.sql += ')'


class IsNull(UnaryExpression):
    def _render(self, r: RenderResult):
        r.sql += '('
        self.child.render(r)
        r.sql += ' is null)'


class IsNotNull(UnaryExpression):
    def _render(self, r: RenderResult):
        r.sql += '('
        self.child.render(r)
        r.sql += ' is not null)'
