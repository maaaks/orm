from orm.expressions.abstract import Expression, RenderResult


class Sql(Expression):
    def __init__(self, code: str):
        self.code: str = code
    
    def _render(self, r: RenderResult):
        r.sql += f'({self.code})'


class Param(Expression):
    def __init__(self, value):
        self.value = value
    
    def _render(self, r: RenderResult):
        n = r.add_param(self.value)
        r.sql += f'${n}'
