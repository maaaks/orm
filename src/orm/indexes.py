from orm.expressions.abstract import Expression


class Index:
    def __init__(self, *expressions: Expression, name: str = None):
        self.name: str = name
        self.expressions: tuple[Expression] = tuple(expressions)


class PrimaryKey(Index):
    pass
