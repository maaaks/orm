class Record:
    def __init__(self, *args):
        self._data = args

    def __getitem__(self, index: int):
        return self._data[index]
    
    def __len__(self):
        return len(self._data)
    
    def __iter__(self):
        return iter(self._data)

    def __repr__(self):
        return 'Record(' + ', '.join(map(repr, self._data)) + ')'

    def __eq__(self, other):
        if isinstance(other, Record):
            return self._data == other._data
