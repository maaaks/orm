FROM python:3.10-alpine

RUN apk add gcc musl-dev postgresql postgresql-dev sudo
RUN pip install --no-cache-dir coverage

WORKDIR /app
RUN mkdir src
COPY setup.py .
RUN pip install --no-cache-dir -e .
